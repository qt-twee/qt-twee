"""
Setup
-----

To setup a development installation one may call either pip as 
follows
::
   pip install -e .
 
or execute the setup script directly 
::
   python setup.py develop

The pip version is generally preferred.
"""
from distutils.core import setup
from setuptools import find_packages
from twee import __meta__
import os

setup(
 author               = __meta__.__author__,
 author_email         = __meta__.__email__,
 name                 = __meta__.__project__,
 url                  = __meta__.__website__,
 version              = __meta__.__version__,
 license              = __meta__.__licence__,
 description          = __meta__.__description__,
 long_description     = open(os.path.join(os.path.dirname(__file__), 'README.md')).read(),
 platforms            = 'any',
 use_scm_version      = True, # Disabling this causes trouble for some reason
 setup_requires       = [],
#  install_requires     = [ 'Qt.py',],   # Qt Normalization Package
#  scripts            = [],
 packages             = find_packages(exclude=["tests",".git","docs"]),
#  include_package_data = True,
 package_dir          = {"twee":"twee"},
 package_data         = {"twee":["*.ico"]},
 zip_safe             = True,
 entry_points         = {"gui_scripts":["twee=twee.__main__"],},
# test_suite           = "tests",
#  tests_require      = ['tox'],
#  cmdclass           = {'test': Tox}
 command_options      = {'build_sphinx': {
                          'project': ('setup.py', __meta__.__project__), # Should really pull this from __meta__
                          'version': ('setup.py', __meta__.version()),
                          'release': ('setup.py', __meta__.release()),
                          'source_dir':('setup.py','docs')}},
 classifiers          = [ # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
  "Development Status :: 3 - Alpha",
  "Intended Audience :: Developers",
#   "License :: OSI Approved :: Python Software Foundation License",
  "Natural Language :: English",
  "Operating System :: OS Independent",
  "Programming Language :: Python",
  "Programming Language :: Python :: 2",
  "Programming Language :: Python :: 2.7",
  "Programming Language :: Python :: 3",
  "Programming Language :: Python :: 3.4",
  "Programming Language :: Python :: 3.5",
  "Programming Language :: Python :: 3.6",
  "Topic :: Software Development :: Libraries :: Python Modules",
  "Topic :: Utilities"
 ],
)
