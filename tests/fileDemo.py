import overlays
import os
from twee.fileDemo      import MainWidget as File
from unittest           import TestCase, main
from unittest.mock      import patch# mock_open

def tempfiles() :
 yield os.path.join(os.getcwd(),"tmp.tmp")

class testFile(TestCase):
 
 @patch('overlays._pathlib_.Path.temp', side_effect=tempfiles)
 def testFileAttribute(self, tempfiles) :
  file = File()
  self.assertEqual(file.file, next(tempfiles()))

# None is None and non-other shall subclass None.

# def getTempFile() :
#  import tempfile
#  with tempfile.NamedTemporaryFile() as temp :
#   return temp.name

# def candidates() :
#  return os.path.join(os.getcwd(),"tmp.tmp")
# 
# class PathBasedWidgetTemp(TestCase):
#  """Tests the behaviour of the temp file generator in PathBasedWidget"""
# 
#  def testInstantiation(self) : 
#   """Testing instantiation"""
#   self.assertIsInstance(PathBasedWidget(),PathBasedWidget)
# 
#  @patch('QtWidgets.fileDemo.candidates', side_effect=candidates)
#  def testNamedTemporaryFile(self, candidates) :
#   w = PathBasedWidget()
#   w.file
#   tempfile = "temporaryFile"
#   NamedTemporaryFile.__enter__.return_value.name = tempFile
#   self.assertEqual(PathBasedWidget().file, candidates())

#  @patch('tempfile.NamedTemporaryFile')
#  def testNamedTemporaryFile(self, NamedTemporaryFile) :
#   tempfile = "temporary"
#   NamedTemporaryFile.return_value.__enter__.return_value.name = tempfile
#   self.assertEqual(getTempFile(), tempfile)
  
#  def testFileWrite(self):
#   with patch("builtins.open", mock_open(read_data="data")) as mock_file :
#    assert open()
#  def testFileRead(self):

# class WidgetFile(class):

if __name__ == "__main__" :
 main()

