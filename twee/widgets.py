from Qt.QtWidgets import QApplication
from Qt.QtWidgets import QMainWindow, QWidget, QMdiArea
from Qt.QtWidgets import QStyle
from Qt.QtWidgets import QAction
from Qt.QtWidgets import QMessageBox
from Qt.QtWidgets import QFileIconProvider
from Qt.QtCore    import Slot
from Qt.QtCore    import QFileInfo
from collections  import Counter
from functools    import partial
from .utilities   import extentions
from .containers  import Format

class MainMDIWindow(QMainWindow) : # , metaclass = QtABCMeta) :
 """Primary window in a Twee application
 
 This class, in conjunction with :class:`MainMDIWidget`, manages the domain specific widgets comprising a Twee application.
 """
 # Deprecated : Twee now uses entrypoints
#  @property
#  def widgets(self):
#   """Returns a list of unique widget/format pairs
#   """
#   return [WidgetFormat(widget, format) for widget in MainWidget.widgets for format in widget.formats]

 #: Qt icon generator for the MainWindow
 iconProvider = QFileIconProvider()

 @property
 def path(self):
  """Determines and returns a default path
  
  This defaults to the root folder of the active widget or the current working directory.
  """
  # This should be defined within the widget
  main = self.main.activeSubWindow() or self.main.currentSubWindow()
  if hasattr(main,"path") :
   return main.path.parent if main.path.is_file() else main.path # This should either be mapped to widget.path or widget.root or widget.file.parent
#   if hasattr(self, "_path_") :
#    return self._path_
  else :
   return Path.cwd() # or the users home directory ?

#  @path.setter
#  def path(self, path):
#   # This should be defined within the widget
#   self._path_ = Path(path)

#  @abstractmethod
#  list(chain.from_iterable(extentions(__package__).values()))
 def __init__(self, *args, paths = [], formats = [item for list in extentions(__package__).values() for item in list], switch = True, **kvps) :
  """Initilaizes the MainWindow
  
  Accepts:
   switch  : A flag that makes the application switch to a secondary screen, useful during development
   widgets : A list of MainWidgets that the application may use to present different file formats
   paths   : A list of files to open and present
  """
  super().__init__(*args, **kvps)
  # Screen
  if switch : self.switch()
  # Formats
#   self.formats = [item for list in extentions(__package__).values() for item in list] # Originally : [WidgetFormat(widget, format) for widget in widgets for format in widget.formats]
#   print(self.formats)
  self.formats = formats
  # Tool bar
  self.file = self.menuBar().addMenu("&File")
  self.tbar = self.addToolBar("Main")
  # Open
#   style = self.style()
#   self.exit = self.addAction(QIcon(style.standardPixmap(QStyle.SP_DialogCloseButton)), "E&xit")
#   self.exit = self.addAction(QIcon(style.standardPixmap(QStyle.SP_TitleBarCloseButton)), "E&xit")
  self.file.open = QAction(self.style().standardIcon(QStyle.SP_DialogOpenButton), '&Open', self)
  self.file.open.setShortcut("Ctrl+O")
  self.file.open.setStatusTip("Open file")
  self.file.open.triggered.connect(self.open)
  self.file.addAction(self.file.open)
  self.tbar.addAction(self.file.open)
  # New
  # Action superceded by submenu
#   self.file.new = QAction(self.style().standardIcon(QStyle.SP_FileIcon),'&New',self)
#   self.file.new.setShortcut("Ctrl+N")
#   self.file.new.setStatusTip("New file")
#   self.file.new.triggered.connect(self.new)
#   self.file.addAction(self.file.new)
#   self.tbar.addAction(self.file.new)
  self.file.new = self.file.addMenu(self.style().standardIcon(QStyle.SP_FileIcon), "&New")
  for index, format in enumerate(sorted(self.formats, key = lambda item : item.format)) :
#   for format, widget in [(format, widget) for widget in self.widgets for format, exts in widget.formats] :
#    formats = {"{} ({})".format(format.description, ",".join(format.extentions)) : widget for widget, format in self.widgets}
   # Delegate action construction to a widget Class method.
   # widget.icon()
   # action = widget.action(self) # Perhaps widget. createAction
   if hasattr(format.widget, "action") :
    action = format.widget.action
   elif hasattr(format.widget, "icon") :
    action = QAction(format.widget.icon , format.description(), self)
   elif format.default is not None : 
    icon   = self.iconProvider.icon(QFileInfo(format.extention()))
    action = QAction(icon, format.description(), self) if icon else QAction(format.description(), self)
   else :
    action = QAction(format.description(), self)
   action.triggered.connect(partial(self.new, format))
   self.file.new.addAction(action)
  # Save
  self.file.save = QAction(self.style().standardIcon(QStyle.SP_DialogSaveButton),'&Save',self)
  self.file.save.setShortcut("Ctrl+S")
  self.file.save.setStatusTip("Save file")
  self.file.save.triggered.connect(self.save)
  self.file.addAction(self.file.save)
  self.tbar.addAction(self.file.save)
  # Save As
  self.file.saas = QAction(self.style().standardIcon(QStyle.SP_DriveFDIcon),'Save &As',self) # QStyle.SP_FileDialogEnd or QStyle.SP_FileDialogStart
  self.file.saas.setShortcut("Alt+Shift+S")
  self.file.saas.setStatusTip("Save under alternative file")
  self.file.saas.triggered.connect(self.saveAs)
  self.file.addAction(self.file.saas)
  self.tbar.addAction(self.file.saas)
  # Quit
  self.file.quit = QAction(self.style().standardIcon(QStyle.SP_DialogCloseButton),'&Quit',self)
  self.file.quit.setShortcut("Ctrl+Q")
  self.file.quit.setStatusTip("Exit application")
  self.file.quit.triggered.connect(QApplication.quit)
  self.file.addAction(self.file.quit)
  # New
#   self.tbar = QToolBar(self)
#   self.addToolBar(self.tbar)
#   self.tbar.new = self.tbar.addAction("New")
#   self.tbar.new.triggered.connect(self.newRow)  
#   self.tbar.new = self.tbar.addAction("Open")
#   self.tbar.new.triggered.connect(self.open)  
  # Widget
  self.main = QMdiArea(self)
  self.main.setViewMode(QMdiArea.TabbedView) # TabbedView or 
  self.main.setActivationOrder(QMdiArea.StackingOrder) # CreationOrder or StackingOrder or ActivationHistoryOrder
#   self.main.setTabShape(QTabWidget.Rounded) # Rounded or Triangular
  self.main.setTabsMovable(True) # True or False
  self.main.setTabsClosable(True) # True or False
  self.setCentralWidget(self.main)
  # Timers
  # Counters
  self.counter = Counter() # Counts the number of MainWidget instances of a given Format
  # Widgets
  self.show()
  for path in paths :
   self.open(path)

 def switch(self, screen = None, fullscreen = True) :
  """
  Places the program upon a second screen for convenience during development
  """
  if screen :
    raise NotImplementedError("Please implement the code necessary to switch screens as and when monitors are added or removed")
  else :
    QApp  = QApplication
    # Screen Size
    XYWH  = QApp.desktop().screenGeometry(QApp.desktop().screenNumber() + 1 % QApp.desktop().screenCount())
    self.resize(XYWH.width()/2, XYWH.height()/2)
    # Window Size
    xywh  = self.geometry()
    self.move(XYWH.x() + XYWH.width()/2 - xywh.width()/2, XYWH.y() + XYWH.height()/2 - xywh.height()/2)

 @Slot(Format)
 def new(self, format): 
  """Create a new document

  .. todo :: 
  
     Accomodate the scenario where the widget has not been specified that shows the menu under new within it's own widget.
     Perhaps consider a different representation for new ?
  """
  self.counter[format.format] += 1 # Consider a default dict ?
  widget = format.widget(self)
  window = MainMDIWidget(self.main)
  print(widget.parent())
  window.setWidget(widget)
  print(widget.parent())
  # Perform these tasks in setWidget
  window.setWindowTitle("_".join("{}-{}".format(format.format, self.counter[format.format]).split()))
#   window.setWindowFilePath(str(file))
#   window.setWindowIcon(self.iconProvider.icon(QFileInfo(str(widget.file))))
#   window.setWindowIconText(str(file.name))
#   self.main.addSubWindow(window)
#   self.main.setActiveSubWindow(window)
#   self.main.setCurrentSubWindow(window)
  window.show()

#  @abstractmethod
 def open(self, path = None) :
  """Review an existing document
  """
  # When the format is unspecified, as when one specifies the file argument, the widgets are selected first by their ORDER then by the DEFAULT file EXTENTION (Note : "*.*" preceeds "*.EXTENTION" in this case)
  # 
  formats = {"{} ({})".format(format.format, ",".join(format.extentions)) : format for format in sorted(self.formats, key = lambda item : (item.format, item.format.extention()))}
  if path : # Path supplied by command line argument
   path = Path(path).resolve()
   if path.is_dir() : # [1] This is very similar to the code where path is unknown
    widgets = sorted(self.widgets, key = lambda item : (item.format.format))
    formats = {"{} ({})".format(format.format, ",".join(format.extentions)) : widget for widget, format in self.widgets}
    file, format = QFileDialog.getOpenFileName(self,"Open File", str(self.path()), ";;".join(sorted(formats.keys())))
    file, widget = Path(file).resolve(), formats.get(format, None)
   else :
    file    = path
    widgets = sorted(self.widgets, key = lambda item : (item.format.order, "~" if item.format.default is None else item.format.extentions[item.format.default], sorted(item.format.extentions)))
    widget  = next((widget for widget, format in widgets if any(fnmatch(file.suffix, ext) for ext in format.extentions)), None)
  elif self.widgets : # [1] This is very similar to the code under path.is_dir # Path supplied by the user via the filedialog
   print(self.widgets)
   widgets = sorted(self.widgets, key = lambda item : (item.format.format, item.format.order))
   formats = {"{} ({})".format(format.format, ",".join(format.extentions)) : widget for widget, format in self.widgets}
   file, format = QFileDialog.getOpenFileName(self,"Open File", str(self.path), ";;".join(sorted(formats.keys()))) # Ideally self.path should be replaced with the widget.path that is the path the current the widget, if any suggests
   file, widget = Path(file).resolve(), formats.get(format, None)
  else :              # Now widgets are presently defined
   msg = QMessageBox(self)
   msg.setIcon(QMessageBox.Information)
   msg.setText("No formats are registered with the application")
   msg.setInformativeText("The MainWindow must be configured to work with atleast one Formst (See Usage:Main Scripts)")
   msg.setDetailedText("Detail")
   msg.setWindowTitle("Configure Main Window/Widget(s)")
   msg.setStandardButtons(QMessageBox.Ok)
   msg.setDefaultButton(QMessageBox.Ok)
   msg.setEscapeButton(QMessageBox.Ok)
   msg.show()
   msg.buttonClicked.connect(lambda value:print(value))
   file = None
  if file and widget :
   try :
    self.statusBar().showMessage("Loading {}".format(file))
    logging.info("Loading {}".format(file))
#     print([(win, win.windowTitle()) for win in self.main.subWindowList()])
    temp = next((widget for widget in self.main.subWindowList() if widget.file == file), None)
    if temp : 
     # Originally this was meant to reload the file, currently it simply activates the widget
#      print(self.main.activeSubWindow())
#      print(self.main.currentSubWindow().widget().file)
     self.main.setActiveSubWindow(temp)
#      temp.show()
#      print(self.main.currentSubWindow().widget().file)
     self.statusBar().showMessage("Activated {}".format(file))
     logging.info("Activated {}".format(file))
    else :
     widget = widget(self)
     widget.load(str(file))
     window = MainMDIWidget(self.main)
     window.setWidget(widget)
     # Perform these tasks in setWidget
     window.setWindowTitle(str(file.name))
     window.setWindowFilePath(str(file))
     window.setWindowIcon(self.iconProvider.icon(QFileInfo(str(widget.file))))
     window.setWindowIconText(str(file.name))
     self.main.addSubWindow(window)
#      self.main.setActiveSubWindow(window)
     window.show()
     self.statusBar().showMessage("Loading {}".format(file))
     logging.info("Loading {}".format(file))
   except KeyError:
    self.statusBar().showMessage("Failed {}".format(file))
    logging.info("Failed {}".format(file))
  else : 
   # To my knowledge we only end up here when the user has cancelled
   self.statusBar().showMessage("Cancelled")
#   if file :
#    widget = next(fnxn for fnxn, frmt, exts)
#   else :
#    formats self.widgets

#  @abstractmethod
#  @slot()
 def save(self) : 
  """Update an existing document
  """
  return (self.main.activeSubWindow() or self.main.currentSubWindow()).save()
#  try :
#   self.main.widget(index).save() if index else self.main.currentWidget().save() 
#  except IOError :
#   self.saveAs(index)

#  @abstractmethod
 def saveAs(self, file = None) :
  """Create a copy of the current document
  """
  return (self.main.activeSubWindow() or self.main.currentSubWindow()).saveAs(file = file)
#  widget = self.main.widget(index) if index else self.main.currentWidget()
#  formats = {"{} ({})".format(frmt,",".join(exts)) : widget for frmt, exts in widget.formats}
#  file, format =  QFileDialog.getSaveFileName(self,"Save File", str(self.path()), ";;".join(sorted(formats.keys())))
#  file, widget = Path(file).resolve(), formats.get(format, None)
#  try : 
#   widget.saveAs(file)
#  except Exception as error :
#   logging.error(error)
#   raise error

#  def saveAll(self) :
#   for window in self.subWindowList() :
#    window.save()

 # Move this to the MDI subclass or the Main window main class
#  def addSubWindow(self, widget, *args, **kvps) :
#   print("Suwin")
#   if   isinstance(widget, MainMDIWindow) : 
#    window = widget
#   elif isinstance(widget, QMdiSubWindow) : 
#    window = MainMDIWindow(self, *args, **kvps) # Use widget.windowflags as an argument
#    window.setWidget(widget.widget())
#   else :
#    print("SubWidget")
#    window = MainMDIWindow(self, *args, **kvps)
#    window.setWidget(widget)
#   return super().addSubWindow(window, *args, **kvps)

if __name__ == "__main__" :
 import sys
 # Application
 app = QApplication(sys.argv)
 # MainWindow
 win = MainMDIWindow()
 win.show()
 # Event Loop
 sys.exit(app.exec_())
