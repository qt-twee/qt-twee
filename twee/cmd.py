"""
The command line interface is implemented using the :class:`QtCommandLineParser` and made available as :attr:`parser`.

.. The Sphinx ArgParse and AutoProgramOutput extentions can't be used for QtCommandLineParser
.. .. argparse::
..    :ref: twee.cmd.parser
..    :prog: twee
"""
from . import __meta__

# DocOpt :
#
# cmd = \
# """
# Usage:
# twee [PATHS...]
# twee -h|--help
# twee --version
# 
# Arguments:
# PATHS      Creates or loads the specified file paths when twee starts up
# 
# Options :
# -h --help  Shows this screen
# --version  Shows the version
# """

# ArgParse :
#
# import __main__             # See : https://doughellmann.com/blog/2012/04/30/determining-the-name-of-a-process-from-python/
# import sys 
# from argparse import ArgumentParser, REMAINDER
# from pathlib  import Path
#
# parser = ArgumentParser(
#  prog = __main__.__package__ or __package__, # Originally __meta__.__project__; Ideally : (__main__.__package__ or __package__).title().replace(".","-"),
# #  prefix_chars = '/', # Windows compatability ?
# #  fromfile_prefix_chars='config@',
#  description ="""
# Qt-twee is a generalized file editor, supported formats are dependent upon the 3rd party packages installed upon ones system.
#  """,
#  epilog      = """
# Related packages are listed under the Packages section of the Qt-Twee documentation.
#  """,)
# # Preferred implementation for PATHS
# # parser.add_argument("paths",
# #  type = Path, nargs="*",
# #  action="store",
# #  dest="paths"
# #  help="one or more files to be loaded once the program is open")
# # Current implementation for PATHS
# parser.add_argument("paths",
#  type = Path,
#  nargs=REMAINDER, 
#  action="store", 
#  help="list of paths to open once the application has loaded")
# # Verbose Argument
# # parser.add_argument("-v", "--verbose", 
# #  action="store_true", # Alternatively : action="count", default=0,
# #  help="set the level of verbosity for application output") # choices=[,,,]
# parser.add_argument("--version",
#  action  = "version",
#  version =f"%(prog)s {__meta__.version()}")
# parser.add_argument("-f", "--formats",
#  action="store_true",
#  help="list the currently supported file formats")

# Qt Command Line:
#
from PyQt5.QtCore import QCommandLineParser, QCommandLineOption, QCoreApplication
#
parser = QCommandLineParser()
parser.setApplicationDescription(__meta__.__description__)
parser.addHelpOption()
parser.addVersionOption()
parser.addPositionalArgument("paths", QCoreApplication.translate("main","list of paths to open once the application has loaded"))
formatList = QCommandLineOption(["f","formats"], QCoreApplication.translate("main", "list the currently supported file formats"));
parser.addOption(formatList)
excludeList = QCommandLineOption(["e","exclude"], QCoreApplication.translate("main", "Exclude certain third party packages"));
parser.addOption(excludeList)
includeList = QCommandLineOption(["i","include"], QCoreApplication.translate("main", "Include certain third party packages"));
parser.addOption(includeList)

