__company__      = "Manaikan"
__website__      = "www.manaikan.com"
__project__      = "Qt-Twee"
__release__      = (0,0,0)
__version__      = __release__[:-1]
__licence__      = "Undecided : please ask"
__author__       = "Carel van Dam"
__email__        = "carelvdam@gmail.com"
__description__  = "A collection of Qt Widgets that are common to a bunch of ones packages"

def version() :
 return ".".join(str(i) for i in __version__)
 
def release() :
 return ".".join(str(i) for i in __release__) 