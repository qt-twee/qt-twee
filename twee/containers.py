from collections import namedtuple

class Format():
 """Denotes a file format
 
 This is a simple class that documents the format its mimetype and supported extentions"""

 @property
 def extention(self) : 
   return None if self.extn is None else (self.extn if self.extn in self.extentions else self.extentions[self.extn])

 def __init__(self, name, mime, exts = [], extn = 0, order = -1) :
  super().__init__()
  self.name = description
  self.mime = mimetype
  self.exts = extentions
  self.extn = default
  self.sort = order

 def description(self):
   return  "{} ({})".format(self.name, self.mimetype, ",".join(self.extentions))

 def __str__(self) :
   return "{} [{}] ({})".format(self.name, self.mimetype, ",".join(self.extentions))
