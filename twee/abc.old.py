"""
Abstract Base Classes (Deprecated)
==================================

Original implementation of Twee, it sets up a QTabWidget within the MainWindow that manages each MainWidget.
It is superceeded by the newer implementation using MDI areas and widgets.
"""

class QtABCMeta(QtMeta, ABCMeta) :
 """
 A Metaclass subclass combining QtMeta and ABCMeta into a single class.
 """

class MainWidget(QWidget,metaclass = QtABCMeta) :
 """
 An Abstract Base Class for widgets that are to be used with :class:`MainWindow`.
 
 This class enforces the protocol expected by :class:`MainWindow` upon its subclasses.
 The protocol enforces the implementation of the following methods :
 
  __init__ - An initialization function.
  load     - A function to load a file or alternatively a set of data.
  save     - A function to save the widgets data to a file.
  saveAs   - A function to save the widgets data to an alternate file.
  
 Default implementations for these methods exist already and the user may simply amke the appropriate :meth:`super` call as follows
 ::
   def FUNCTION(self, *args, **kvps):
    super().FUNCTION(*args, **kvps)
  
 substituting FUNCTION with the relevant method name.
 
 The trick when subclassing this class is to make self.data into a property.
 This allows one to handle any data convertion necessary and assign the data to the appropriate subcomponent for ones main widget.
 The default implementations of :meth:`load`, :meth:`save` and  :meth:`saveAs` simply assign the data passed to them or the contents of a file which name is passed to them to the classes :attr:`data` attribute.
 One may leverage Pythons' property decorator do perform any necessary manipulations to the data as it's set or retrieved.
 ::
   @property
   def data(self) :
    return self._data_
   @data.setter
   def data(self, data):
    self._data_ = data

 .. todo ::
 
    Ideally MainWindow would register subclasses of this class and use this list as the widget list.
 """
 @abstractmethod
 def __init__(self, *args, source = None, **kvps) :
  super().__init__(*args, **kvps)
  if source : self.load(source)

 @abstractmethod
 def load(self, source = None):
  """
  The following example implementation shold be used that accounts for various inputs.
  ::
     def load(self, source = None):
      load = lambda file : file.read()
      if isinstance(source, (Path, WindowsPath)):
       with source.open('r') as file :
        self.data, self.file = load(file), source
      elif isinstance(source, str) and os.path.exists(source):
       with open(source,'r') as file :
        self.data, self.file = load(file), Path(source)
      elif isinstance(source, str) :
       self.data, self.file = source, None
      else : # At this point we have something other then text.
       self.data, self.file = source, None
  """
#   raise NotImplemnetedError("Loading is not presently implemented")
  if isinstance(source, (Path, WindowsPath, PosixPath)): # Other classes to consider PurePosixPath, PureWindowsPath, PurePath (This seems to be the common decendant e.i. PuePath may be substituted for the list of classes)
   with source.open('r') as file :
    self.data, self.file = file.read(), source
  elif isinstance(source, str) and os.path.exists(source):
   with open(source,'r') as file :
    self.data, self.file = file.read(), Path(source)
#   elif isinstance(source, str) :
#    self.data, self.file = source, None
  else : # At this point we have something other then text.
   self.data, self.file = source, None
  
 @abstractmethod
 def save(self):
  """
  The following provides an example implemenetation. Note the 
  roll over into self.saveAs when the file is not specified.
  ::
    def save(self) :
     if hasattr(self, 'file') and self.file : 
      with open(self.file,'w') as file :
       file.write(self.data)
      return True 
     else :
      return self.saveAs()  
  """
#   raise NotImplemnetedError("Saving is not presently implemented")
  #if hasattr(self, 'file') and self.file : 
  try : 
   with open(self.file,'w') as file :
    file.write(self.data)
  except Exception as error:
   raise error # as IOError("The save function failed to store ones data")
  #else :
  # return False

 @abstractmethod
 def saveAs(self, target = None):
  """
  Implmenets the Save as method.
  """
  raise NotImplemnetedError("Exporting is not presently implemented")
  
class MainWindow(QMainWindow, metaclass = QtABCMeta) :
 """Main Window
 
 This provides the methods 
 
   create - equivalent to new
   update - equivalent to save
   review - equivalent to open
   
 Users subclassing this class must implement their own version of route, which maps file types to loader functions.
 These loader functions must open a file of the specified type and append a widget for viewing it to the central widget self.cntr
 
 .. todo :: 
 
    Formats should be specified by the MainWidget and not MainWindow,
    Mainwindow should iterate over the supported MainWidgets and the
    formats determined accordingly.
 """

 @abstractmethod
 def __init__(self, *args, files = [], **kvps) :
  super().__init__(*args, **kvps)
  # Icons
#   self.syms = QFileIconProvider()
  # Tool bar
  # Widget
  self.main = QMDIWidget(self)
#   self.main.setDocumentMode(True)
#   self.main.setMovable(True)
#   self.main.setTabsClosable(True)
#   self.main.tabCloseRequested.connect(self.close)
#   self.main.currentChanged.connect(self.active)
#   self.main.setTabShape(QTabWidget.Triangular)
  self.setCentralWidget(self.main)
  # Timers
  # Screen
  self.switch()
  # Files
  for path in files : #: todo : merge this with the open method some day
   self.open(Path(path).resolve())

 def switch(self, screen = None, fullscreen = True) :
  """
  Places the program upon a second screen for convenience during development
  """
  if screen :
    raise NotImplementedError("Please implement the code necessary to switch screens as and when monitors are added or removed")
  else :
    QApp  = QApplication
    # Screen Size
    XYWH  = QApp.desktop().screenGeometry(QApp.desktop().screenNumber() + 1 % QApp.desktop().screenCount())
    self.resize(XYWH.width()/2, XYWH.height()/2)
    # Window Size
    xywh  = self.geometry()
    self.move(XYWH.x() + XYWH.width()/2 - xywh.width()/2, XYWH.y() + XYWH.height()/2 - xywh.height()/2)

 @abstractmethod
 def new(self, widget) :
  """Create a new document
  """
  temp = widget(self)
  icon = None # self.fileIcon(QFileInfo(str(file)))
  if icon : # https://stackoverflow.com/a/45739529
   self.main.addTab(temp, icon, "New File")
  else :
   self.main.addTab(temp, "New File")

 @abstractmethod
 def open(self, file = None) :
  """Review an existing document
  """

 @abstractmethod
 def save(self) :
  """Update an existing document
  """
  try :
   self.main.widget(index).save() if index else self.main.currentWidget().save() 
  except IOError :
   self.saveAs(index)

 @abstractmethod
 def saveAs(self) :
  """Create a copy of the current document
  """
  widget = self.main.widget(index) if index else self.main.currentWidget()
  formats = {"{} ({})".format(frmt,",".join(exts)) : widget for frmt, exts in widget.formats}
  file, format =  QFileDialog.getSaveFileName(self,"Save File", str(self.path()), ";;".join(sorted(formats.keys())))
  file, widget = Path(file).resolve(), formats.get(format, None)
  try : 
   widget.saveAs(file)
  except Exception as error :
   logging.error(error)
   raise error
