"""
Various utility functions that are useful across the library
"""
import pkg_resources
from collections import OrderedDict, Mapping

def extentions(entry_point = __package__, load = True, container=OrderedDict()) :
 """
 Iterate over the extentions identified by `entrypoint` collecting collecting the registered classes into a dictionery.
 
 Accepts:
  entrypoint : The entrypoint to scan
  load       : Optionally loading the package/module/class during iteration or leaving this to the user
  container  : A list/dictionery into which the items are respectively stored [Default : OrderedDict()]
  
 Returns:
  container  : The container passed to the function populated with the required entrypoints.
  
 Notes:
  * Since each entry point is self contained there it is perhaps better to return a list 
  
 To do:
  * Make load false by default
 """
 if isinstance(container, Mapping):
   for item in sorted(pkg_resources.iter_entry_points(entry_point), key = lambda item : item.name):
#     try : 
     container[item.name] = item.load() if load else item # It would be more efficient to only load these classes when they are required
#     except ImportError: 
 else : 
   for item in pkg_resources.iter_entry_points(entry_point) :
#   try : 
     container.extend(item.load()  if load else item)
#   except ImportError: 
   print("Un tested please confirm you see a list of entry_points", formats)
 return container
  
def _extentionsList_(entry_point = __package__) :
 """
 Deprecated : This was originally meant to expand the container of entry_points but I can't recall why I might have done so, probably laziness.
 
 This is redundant, but it seems one may not expand the values of a dictionery within a functions' argument specification.
 That is ARGUMENT does not receive a list ::
  
   def METHOD(ARGUMENT=extentions(_package__).values()) :
  
 but a :class:`dict_values` instance and ones efforts to expand this within the arguments specification ::
  
   reduce(concat, extentions(_package__).values())
   sum(extentions(_package__).values(), [])
   [item for list in extentions(__package__).values() for item in list]
   list(chain.from_iterable(extentions(_package__).values()))
 
 have all failed. The Python compiler appears to be performing some sort of optimization that disregards the expansion.
 
 Accepts:
  entrypoint : The entrypoint to scan
  
 Returns:
  formats    : A list of ``Format`` instances
 """
 raise ValueError("Deperecated")
 
__all__ = ["extentions"]
