from PyQt5.QtWidgets import QApplication, QMainWindow, QStatusBar, QTabWidget, QAction, QFileDialog, QStyle, QWidget, QFileIconProvider, QMenu
from PyQt5.QtGui     import QIcon
from PyQt5.QtCore    import QFileInfo
from pathlib import Path
import logging
import os
from fnmatch import fnmatch

# from PyQt5.QtCore import qApp

class MainWindow(QMainWindow):
 """Main Window
 
 This provides the methods 
 
   **create** 
    equivalent to new
   **update** 
    equivalent to save
   **review**
    equivalent to open
   
 Users subclassing this class must implement their own version of route, which maps file types to loader functions.
 These loader functions must open a file of the specified type and append a widget for viewing it to the central widget self.cntr
 
 Todo: 
 
  Formats should be specified by the MainWidget and not MainWindow,
  Mainwindow should iterate over the supported MainWidgets and the
  formats determined accordingly.
 """

 def __init__(self, *args, files = [], **kvps) : 
  logging.info("MainWindow")
  super().__init__(*args, **kvps)
  # Tool bar
  self.file = self.menuBar().addMenu("&File")
  self.tbar = self.addToolBar("Main")
  # Open
#   style = self.style()
#   self.exit = self.addAction(QIcon(style.standardPixmap(QStyle.SP_DialogCloseButton)), "E&xit")
#   self.exit = self.addAction(QIcon(style.standardPixmap(QStyle.SP_TitleBarCloseButton)), "E&xit")
  self.file.open = QAction(self.style().standardIcon(QStyle.SP_DialogOpenButton), '&Open', self)
  self.file.open.setShortcut("Ctrl+O")
  self.file.open.setStatusTip("Open file")
  self.file.open.triggered.connect(self.open)
  self.file.addAction(self.file.open)
  self.tbar.addAction(self.file.open)
  # New
#   self.file.new = QAction(self.style().standardIcon(QStyle.SP_FileIcon),'&New',self)
#   self.file.new.setShortcut("Ctrl+N")
#   self.file.new.setStatusTip("New file")
#   self.file.new.triggered.connect(self.new)
#   self.file.addAction(self.file.new)
#   self.tbar.addAction(self.file.new)
  self.file.new = self.file.addMenu(self.style().standardIcon(QStyle.SP_FileIcon), "&New")
  for format, widget in [(format, widget) for widget in self.widgets for format, exts in widget.formats] :
   # Delegate action construction to a widget Class method.
   # widget.icon()
   # action = widget.action(self) # Perhaps widget. createAction
   action = QAction(format, self)
   action.triggered.connect(lambda : self.new(widget))
   self.file.new.addAction(action)
  # Save
  self.file.save = QAction(self.style().standardIcon(QStyle.SP_DialogSaveButton),'&Save',self)
  self.file.save.setShortcut("Ctrl+S")
  self.file.save.setStatusTip("Save file")
  self.file.save.triggered.connect(self.save)
  self.file.addAction(self.file.save)
  self.tbar.addAction(self.file.save)
  # Save As
  self.file.saas = QAction(self.style().standardIcon(QStyle.SP_DriveFDIcon),'Save &As',self) # QStyle.SP_FileDialogEnd or QStyle.SP_FileDialogStart
  self.file.saas.setShortcut("Alt+Shift+S")
  self.file.saas.setStatusTip("Save file as")
  self.file.saas.triggered.connect(self.saveAs)
  self.file.addAction(self.file.saas)
  self.tbar.addAction(self.file.saas)
  # Quit
  self.file.quit = QAction(self.style().standardIcon(QStyle.SP_DialogCloseButton),'&Quit',self)
  self.file.quit.setShortcut("Ctrl+Q")
  self.file.quit.setStatusTip("Exit application")
  self.file.quit.triggered.connect(QApplication.quit)
  self.file.addAction(self.file.quit)
  # New
#   self.tbar = QToolBar(self)
#   self.addToolBar(self.tbar)
#   self.tbar.new = self.tbar.addAction("New")
#   self.tbar.new.triggered.connect(self.newRow)  
#   self.tbar.new = self.tbar.addAction("Open")
#   self.tbar.new.triggered.connect(self.open)  
  # Widget
  self.main = QTabWidget(self)
  self.main.setDocumentMode(True)
  self.main.setMovable(True)
  self.main.setTabsClosable(True)
  self.main.tabCloseRequested.connect(self.close)
  self.main.currentChanged.connect(self.active)
  self.syms = QFileIconProvider()
#   self.main.setTabShape(QTabWidget.Triangular)
  self.setCentralWidget(self.main)
  # Timers
  # Widgets
  # Screens
  self.switch()
  # Display
#   self.showMaximized()
  # Status bar
  self.statusBar().showMessage("Started")
  # Loading
  for path in files : #: todo : merge this with the open method some day
   self.open(Path(path).resolve())

 def switch(self, screen = None, fullscreen = True) :
  """
  Places the program upon a second screen for convenience during development
  """
  if screen :
    raise NotImplementedError("Please implement the code necessary to switch screens as and when monitors are added or removed")
  else :
    QApp  = QApplication
    # Screen Size
    XYWH  = QApp.desktop().screenGeometry(QApp.desktop().screenNumber() + 1 % QApp.desktop().screenCount())
    self.resize(XYWH.width()/2, XYWH.height()/2)
    # Window Size
    xywh  = self.geometry()
    self.move(XYWH.x() + XYWH.width()/2 - xywh.width()/2, XYWH.y() + XYWH.height()/2 - xywh.height()/2)

 def new(self, widget) :
  """
  Create a new instance of the specified widget
  """
  temp = widget(self)
  icon = None # self.fileIcon(QFileInfo(str(file)))
  if icon : # https://stackoverflow.com/a/45739529
   self.main.addTab(temp, icon, "New File")
  else :
   self.main.addTab(temp, "New File")
  
  
 def open(self, file = None) :
  """
  Allows the user to select a file to load
  """
  # One might consider using an OrderedDict should alphabetical 
  # ordering not become suitable.
#  if file is None : 
  if file :
    widget = next((fnxn for fnxn, frmt, exts in self.formats if [fnmatch(file, extn) for extn in exts]), None) # It may seem prudent to replace None with a TextEdit but the user should really allow for *.* patterns in this case.
  else :
    formats = {"{} ({})".format(format,",".join(exts)) : widget for widget in self.widgets for format, exts in widget.formats}
#     formats = {"{} ({})".format(frmt,",".join(exts)) : fnxn for fnxn, frmt, exts in self.formats}
    file, format = QFileDialog.getOpenFileName(self,"Open File", str(self.path()), ";;".join(sorted(formats.keys())))
    file, widget = Path(file).resolve(), formats.get(format, None)
  if file and widget :
   try :
    self.statusBar().showMessage("Loading {}".format(file))
    logging.info("Loading {}".format(file))
    try : 
     self.main.setCurrentWidget(
      self.main.widget(
       [self.main.widget(index).file for index in range(self.main.count())].index(file)))
     self.statusBar().showMessage("Reloaded {}".format(file))
     logging.info("Reloaded {}".format(file))
    except ValueError :
     temp = widget(self, source = file)
     icon = self.fileIcon(QFileInfo(str(file)))
     if icon : # https://stackoverflow.com/a/45739529
      self.main.addTab(temp, icon, file.stem)
     else :
      self.main.addTab(temp, file.stem)
     self.statusBar().showMessage("Loading {}".format(file))
     logging.info("Loading {}".format(file))
   except KeyError :
    self.statusBar().showMessage("Failed {}".format(file))
    logging.info("Failed {}".format(file))
  else : 
   # To my knowledge we only end up here when the user has cancelled
   self.statusBar().showMessage("Cancelled")

 def load(self, path = None):
   if path.is_dir() :
    pass # Future versions should open the folder and let the user select a file
   else :
    temp = QGander(self, source = path)
    self.cntr.addTab(temp, path.stem)

#  def load(self, file = None) :
#   """
#   Generic Loader, this is a last resort and one should prefer 
#   one of the dedicated loaders.
#   
#   Currently only loadMySQL is dsetup properly but the intention
#   is to support both PostgresSQL and SQLite eventually aswell.
#   Further dialects are also possible but these would need to be
#   investigated properly.
#   """
#   if file :
#    sql.parseFile(file)
#    for item in :
#     self.model.addWidget(Table("test")) 
#   else :
#    self.model.addWidget(Table("test"))

 def save(self, index = None):
  """Instigate a save event
  
  Here we try to save the users data, if the save method throws
  an IOError we try to perform a save as instead.
  """
  try :
   self.main.widget(index).save() if index else self.main.currentWidget().save() 
  except IOError :
   self.saveAs(index)

 def saveAs(self, index = None):
  """Instigates a Save As event
  
  Should a normal save fail or the user select to save a file 
  under a different name. This triggers a SaveAs dialogue and
  saves the file under a new name.
  """
  widget = self.main.widget(index) if index else self.main.currentWidget()
  formats = {"{} ({})".format(frmt,",".join(exts)) : widget for frmt, exts in widget.formats}
  file, format =  QFileDialog.getSaveFileName(self,"Save File", str(self.path()), ";;".join(sorted(formats.keys())))
  file, widget = Path(file).resolve(), formats.get(format, None)
  try : 
   widget.saveAs(file)
  except Exception as error :
   logging.error(error)
   raise error

 def close(self, index):
  saved = self.save(index) if hasattr(self.main.widget(index),'file') and self.main.widget(index).file is not None else self.saveAs(index)
  if saved : 
   self.main.removeTab(index)

 def active(self, index):
  if hasattr(self.main.widget(index),'file') and self.main.widget(index).file : 
   self.statusBar().showMessage(str(self.main.widget(index).file))
  
 def path(self) :
  """
  Detemine the current working directory. In order of preference
  this should be the folder containing the file in the active tab
  then the folder from which the application was invoked and last
  but not least the folder in which the applciation resides.
  User folders are also to be considered.
  """
  return Path.cwd()
  return Path.home()

 def fileIcon(self, icon) :
  return self.syms.icon(icon)
