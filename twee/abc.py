"""
This module provides the Abstract Base Classes (ABC).
These comprise of the :class:`MainMDIWindow` and :class:`MainMDIWidget`.
:class:`MainMDIWindow` comprises of a thin wrapper around :class:`QMainWindow` and manages the :class:`QMdiArea` which hosts the :class:`MainMDIWidget` instances instantiated during the applications execution.
:class:`MainMDIWidget` subclasses :class:`QMdiSubWindow`.

:class:`MainMDIWindow` and :class:`MainMDIWidget` utilize a common protocol for the manipulation of files.
:class:`MainMDIWindow` creates, locates, opens and closes files with the operating system.
Subclasses of :class:`MainMDIWidget` load and save the data of these files along with the manipulation of the data they contain. 
These subclasses may implement whatever pattern is necessary to manage the Create/Review/Update/Delete operations upon this data.
"""
# import sys
# from pprint import pprint
# print(sys.meta_path)
# print(sys.path)
# print(sys.modules)
# import overlays
from twee.overlay import Path
import os
import logging
from abc     import ABCMeta, abstractmethod
# from .pathlib import Path
# from pathlib import Path
from sip     import wrappertype as QtMeta
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QMdiArea, QMdiSubWindow, QFileIconProvider, QTabWidget, QAction, QStyle, QFileDialog, QMessageBox
from PyQt5.QtCore    import QFileInfo, pyqtSlot as slot, Qt
# from tempfile    import mkstemp
from collections import namedtuple
from fnmatch     import fnmatch
from functools   import partial
from .utilities  import extentions
# print(sys.metapath)

class MainWidget(QWidget) : # , metaclass = QtABCMeta) :
 """Primary widget in a Twee application
 
 This class, in conjunction with :class:`MainMdiWidget`, may be used
 to create domain specific widgets within a Twee Application
 
 note :
  This conflicts with the implementation of the QtSvgRenderer class.
  
 todo :
  Split the class into a proper ABC and a mixin class providing load/save capabilities.
 """

 @property
 @abstractmethod
 def formats(self) :
  """Returns a file type tuple
  
  The file type with a description, list of file types, an index to the default file type and a priority. 
  """
  return [Format(100, "Any file format",["*.*"],0)]

#  @property
#  def format(self):
#   if hasattr(self, '_format_') :
#    return self._format_
   
#  @format.setter
#  def format(self, format):
#   if format in self.formats :
#    self._format_ = format

#  @property
#  @abstractmethod
#  def path(self):
#   try :
#    return self._path_
#   except AttributeError :
#    return None 

#  @path.setter
#  @abstractmethod
#  def path(self, path):
#   self._path_ = path

 @property
 def file(self):
  # Alternative implementation
#   if not hasattr(self, "_file_") :
#    self._file_ = "File"
#   return self._file_ 
  try :
   return self._file_
  except AttributeError :
   return None
  # Implementation with common path
#    if self.path :
# #    self._file_, self._path_ = mkstemp() 
#     self._file_ = self.path.open()
#    else : 
#     self._file_ = temp.NamedTemporaryFile()
  # alternative implementation
#   try : 
#    return self._temp_
#   except AttributeError :
#    self._temp_ = next(temp._get_candidate_names())
#   finally :
#    return self._temp_

 @file.setter
 def file(self, path) : 
  # Not sure if it's prudent to both create and open the file 
  # or if it's sufficient to simply open it ?
  #
  # The algo works as follows 
  #  1) test if self.temp exists
  #     T) move it next to the file
  #     F) create one relative to the file
  #
  assert isinstance(path, Path)
#   if self.temp.exists() :
  self._file_ = path
#   _temp_, temp = path.temp(), self.temp
#   if self._temp_ : # Move any existing temp file to the new location
#    try :
#     os.rename(temp, _temp_)
#     self._temp = _temp_
#    except OSError as error: 
#     # Either delete the file or create a copy.
#     logging.error(error.message)  
#   else : # 
#    self._temp_ = _temp_

# @file.setter
# @abstractmethod
# def file(self, file):
#  assert isinstance(file, (Path, PosixPath, WindowsPath)) # Ensure file name, note that it might not exist yet.
#  assert not file.is_dir()
#  self._file_ = file
##   try : 
##    os.copy(self._file_, file)
## #    file = self._file_
##    # Close existing file
## #    os.close(self._file_)
## #    os.remove(self._path_)
##   except :
## #    self._file_, self._path_ = mkstemp(file.file.path()) 
##   finally :

#  @file.deleter
#  @abstractmethod
#  def file(self):
#   try :
#    return self._file_
#   except AttributeError :
#    self._temp_, se;f._fd_  = 
#   finally : 
#    return self._temp_

 @property
 def temp(self):
  try :
#    print("Accessed temp")
   return self._temp_
  except AttributeError :
#    print("Create temp")
#    print(dir(Path))
   try : # This accounts for a failing in Apeman to consistently substitute a package across modules which import one another in sequence.
	   if self.file :
	    self._temp_ = self.file.temp() # (self.file.parent, self.file.stem, self.file.extn)
	   else :
	    self._temp_ = Path.tmppath() # "test" # Path.tmppath()
   except Exception as error :
    print(error)
    from datetime import datetime
    self._temp_ = os.path.join(os.getcwd(), "temp{}.tmp".format(datetime.now().strftime("%Y%M%dT%H%m%S")))
  finally :
#    print("Returned temp")
   return self._temp_

 @property
 @abstractmethod
 def data(self):
  return self._data_

 @data.setter
 @abstractmethod
 def data(self, data):
  self._data_ = data

 # Handled by MainWindow/MainMDIWidget
#  @property
#  def formats():
#   return [(*format, widget) for widget in self.widgets for format in widget.formats]

#  def __init_subclass__(cls, *args, **kvps) :
#   print("Subclassing MainWidget")
#   super().__init_subclass__(*args, **kvps)
#   cls.widgets.append(cls)

#  @abstractmethod
 def __init__(self, *args, source = None, **kvps) :
  super().__init__(*args, **kvps)
  # self.settings = QSettings("Python","Twee") # "Manaikan","Twee"
  if source : 
   try : 
    self.load(source)
   except ValueError as error :
    self.data = source 

#  @abstractmethod
 def load(self, source) :
  """
  Tries to load source, under the assumption that it is a path, should this fail it raises a ValueError.
  """
  # Consider splitting this into loadFrom and simply load ? In the same way that one has save and saveAs
  if isinstance(source, (Path)): # , WindowsPath, PosixPath # Other classes to consider PurePosixPath, PureWindowsPath, PurePath (This seems to be the common decendant e.i. PuePath may be substituted for the list of classes)
   self.file = source
   with open(self.file, 'r') as file :
    self.data = file.read()
  elif isinstance(source, str) and os.path.exists(source):
   self.file = Path(source).resolve()
   with open(source,'r') as file :
    self.data = file.read()
  else : 
   raise ValueError("Expected a readable path for SOURCE")
   # Originally anydata format was accomodated but it is quite easy for the user to simply assign self.data = data  
#    self.data, self.file = source, None
 
#  @abstractmethod
 def save(self) :
  """
  The following provides an example implemenetation. Note the 
  roll over into self.saveAs when the file is not specified.
  ::
  
    def save(self) :
     if hasattr(self, 'file') and self.file : 
      with open(self.file,'w') as file :
       file.write(self.data)
      return True 
     else :
      return self.saveAs()  
  """
  # Strictly speaking this should really lock and then open 
  # the main file and the temp file, compare their contents 
  # and open up a diff if there is a discrepency beyond the 
  # updates in the temp file, once resolved the both files 
  # ought to be resolved into the same file.
  temp = file = False
  try :
#    print(dir(self))
   print(self.temp)
#    with open(self.temp,'w') as file :
#     file.write(self.data)
#    temp = True
  except Exception as error:
   raise error # as IOError("The save function failed to store ones data")
  try :
#    with open(self.file,'w') as file :
#     file.write(self.data)
   file = True
  except Exception as error:
   raise error # as IOError("The save function failed to store ones data")
  return (temp, file)

#  @abstractmethod
 def saveAs(self, target):
  """
  This gets called when 
  """
  if isinstance(target, (Path, WindowsPath, PosixPath)): # Other classes to consider PurePosixPath, PureWindowsPath, PurePath (This seems to be the common decendant e.i. PuePath may be substituted for the list of classes)
   self.file = target
   return self.save()
  elif isinstance(source, str) and os.path.exists(source):
   self.file = Path(source).resolve()
   return self.save()
  else :
   raise ValueError("Expected TARGET to be a file name or path")
   # Originally I though the user might extract the data through this function but it is far easier for them to access self.data
#    self.data, self.file = source, None

# MainWidget = MainMDIWidget

class MainMDIWidget(QMdiSubWindow) :
 """The MainWidget in a Twee application

 Vendors must subclass this class and implement their interface within it.
 This class, in conjunction with :class:`MainMdiWidget` and :class:`MainWidget`, may be used to manage format specific widgets in Twee.
 It provides encapsulation for :class:`MainWidget` subclasses such that they may be used with :class:`MainMDIWindows`.
 """

# iconProvider = QFileIconProvider()

 # Is it possible to create a class level property or equivalent ?
 @property
 def widgets(self) :
  widget = self.widget() 
  return widget.widgets if widget else None

 @property
 def formats(self) :
  widget = self.widget() 
  return widget.formats if widget else []

 @property
 def file(self) :
  widget = self.widget() 
  return widget.file if widget else None

 @file.setter
 def file(self, path) : 
  widget = self.widget() 
  if widget : widget.file = path

 @property
 def temp(self) :
  widget = self.widget() 
  return widget.temp if widget else None

 def load(self, source = None) : 
  widget = self.widget() 
  if widget : widget.load(source)

 def save(self) : 
  widget = self.widget() 
  if widget : widget.save()

 def saveAs(self, target = None) : 
  widget = self.widget() 
  if widget : widget.saveAs(target)

#  def setWidget(self, widget, *args, **kvps) :
#   window = super().setWidget(widget, *args, **kvps)
#   # Identification
#   if hasattr(widget, "file") and widget.file :
#    window.setWindowTitle(str(file))
#    window.setWindowIconText(str(file))
#    window.setWindowFilePath(str(file))
#   # Icon 
#   #
#   # [1] discusses how to determine the system icon from a file
#   # extention
#   #
#   # [1] https://stackoverflow.com/a/45739529
#   if   hasattr(widget, "icon") :
#    window.setWindowIcon(widget.icon)
#   elif hasattr(widget, "file") and widget.file :
#    window.setWindowIcon(self.iconProvider(QFileInfo(str(widget.file))))
# #   if   hasattr(widget, "title") :
# #    window.setWindowIconText(widget.title)
# #   elif hasattr(widget, "file") and widget.file :
# #    window.setWindowIconText(iconProvider(QFileInfo(str(file))))
#   return window

 def __init__(self, *args, **kvps) :
  super().__init__(*args, **kvps)
  self.setAttribute(Qt.WA_DeleteOnClose)

 def closeEvent(self, event, *args, **kvps) :
  # https://stackoverflow.com/a/8973595
#   self.closed.emit(self)
#   try :
#    self.save()
#   except OSError :
#    self.saveAs() 
  super().closeEvent(event, *args, **kvps)

if __name__ == "__main__" :
 from PyQt5.QtWidgets import QApplication
 import sys
 from docopt import docopt, split, retag, clean, funparts

 cmd = \
 """
 Usage:
 gander [FILES...]
 gander -h|--help
 gander --version
 
 Arguments:
 file       Open the program with the specified file loaded
 
 Options :
 -h --help  Shows this screen.
 --version  Shows the version.
 """
#  class Mainwindow(MainMDIWindow) :
#   windows = [MainMDIWindow]
 app = QApplication(sys.argv)
 # Command line arguments
 ops = docopt(cmd, app.arguments()[1:], help=False, options_first=True) # Need to disbale help and version, perhaps enable arguments first ?
 arg, kvp, rem = split(retag(ops, clean(ops)), *funparts(MainWindow))            # Note : docopt.normalize fails as Mainwindow is not a function but rather a class, so `self` appears in args but is not present in ops.
 # Main Window
 win = MainMDIWindow(**kvp)
 win.show()
#  import time
#  time.sleep(15)
#  win.open("test.json")
 # Loop
 sys.exit(app.exec_()) 