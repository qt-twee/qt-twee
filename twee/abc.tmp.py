class MainWidget(QWidget) :
 """Primary widget in a Twee application
 
 This class, in conjunction with :class:`MainMdiWidget`, may be used
 to create domain specific widgets within a Twee Application.
 """

 widgets = []

 @property
 @abstractmethod
 def formats(self) :
  """Returns a file type tuple
  
  The file type with a description, list of file types, an index to the default file type and a priority. 
  """
  return [Format(100, "Any file format",["*.*"],0)]

#  @property
#  def format(self):
#   if hasattr(self, '_format_') :
#    return self._format_
   
#  @format.setter
#  def format(self, format):
#   if format in self.formats :
#    self._format_ = format

#  @property
#  @abstractmethod
#  def path(self):
#   try :
#    return self._path_
#   except AttributeError :
#    return None 

#  @path.setter
#  @abstractmethod
#  def path(self, path):
#   self._path_ = path

 @property
 def file(self):
  # Alternative implementation
#   if not hasattr(self, "_file_") :
#    self._file_ = "File"
#   return self._file_ 
  try :
   return self._file_
  except AttributeError :
   return None
  # Implementation with common path
#    if self.path :
# #    self._file_, self._path_ = mkstemp() 
#     self._file_ = self.path.open()
#    else : 
#     self._file_ = temp.NamedTemporaryFile()
  # alternative implementation
#   try : 
#    return self._temp_
#   except AttributeError :
#    self._temp_ = next(temp._get_candidate_names())
#   finally :
#    return self._temp_

 @file.setter
 def file(self, path) : 
  # Not sure if it's prudent to both create and open the file 
  # or if it's sufficient to simply open it ?
  #
  # The algo works as follows 
  #  1) test if self.temp exists
  #     T) move it next to the file
  #     F) create one relative to the file
  #
  assert isinstance(path, Path)
#   if self.temp.exists() :
  self._file_ = path
#   _temp_, temp = path.temp(), self.temp
#   if self._temp_ : # Move any existing temp file to the new location
#    try :
#     os.rename(temp, _temp_)
#     self._temp = _temp_
#    except OSError as error: 
#     # Either delete the file or create a copy.
#     logging.error(error.message)  
#   else : # 
#    self._temp_ = _temp_

# @file.setter
# @abstractmethod
# def file(self, file):
#  assert isinstance(file, (Path, PosixPath, WindowsPath)) # Ensure file name, note that it might not exist yet.
#  assert not file.is_dir()
#  self._file_ = file
##   try : 
##    os.copy(self._file_, file)
## #    file = self._file_
##    # Close existing file
## #    os.close(self._file_)
## #    os.remove(self._path_)
##   except :
## #    self._file_, self._path_ = mkstemp(file.file.path()) 
##   finally :

#  @file.deleter
#  @abstractmethod
#  def file(self):
#   try :
#    return self._file_
#   except AttributeError :
#    self._temp_, se;f._fd_  = 
#   finally : 
#    return self._temp_

 @property
 def temp(self):
  try :
#    print("Accessed temp")
   return self._temp_
  except AttributeError :
#    print("Create temp")
#    print(dir(Path))
   try : # This accounts for a failing in Apeman to consistently substitute a package across modules which import one another in sequence.
	   if self.file :
	    self._temp_ = self.file.temp() # (self.file.parent, self.file.stem, self.file.extn)
	   else :
	    self._temp_ = Path.tmppath() # "test" # Path.tmppath()
   except Exception as error :
    print(error)
    from datetime import datetime
    self._temp_ = os.path.join(os.getcwd(), "temp{}.tmp".format(datetime.now().strftime("%Y%M%dT%H%m%S")))
  finally :
#    print("Returned temp")
   return self._temp_

 @property
 @abstractmethod
 def data(self):
  return self._data_

 @data.setter
 @abstractmethod
 def data(self, data):
  self._data_ = data

 @property
 def formats():
  return [(*format, widget) for widget in self.widgets for format in widget.formats]

 def __init_subclass__(cls, *args, **kvps) :
  """
  Registers the MainMDIWidget subclasses 
  """
  super().__init_subclass__(*args, **kvps)
  cls.widgets.append(cls)

#  @abstractmethod
 def __init__(self, *args, source = None, **kvps) :
  super().__init__(*args, **kvps)
  # self.settings = QSettings("Python","Twee") # "Manaikan","Twee"
  if source : 
   try : 
    self.load(source)
   except ValueError as error:
    self.data = source 

#  @abstractmethod
 def load(self, source):
  """
  """
  # Consider splitting this into loadFrom and simply load ? In the same way that one has save and saveAs
  if isinstance(source, (Path)): # , WindowsPath, PosixPath # Other classes to consider PurePosixPath, PureWindowsPath, PurePath (This seems to be the common decendant e.i. PuePath may be substituted for the list of classes)
   self.file = source
   with open(self.file, 'r') as file :
    self.data = file.read()
  elif isinstance(source, str) and os.path.exists(source):
   self.file = Path(source).resolve()
   with open(source,'r') as file :
    self.data = file.read()
  else : 
   raise ValueError("Expected a readable path for SOURCE")
   # Originally anydata format was accomodated but it is quite easy for the user to simply assign self.data = data  
#    self.data, self.file = source, None
 
#  @abstractmethod
 def save(self):
  """
  The following provides an example implemenetation. Note the 
  roll over into self.saveAs when the file is not specified.
  ::
    def save(self) :
     if hasattr(self, 'file') and self.file : 
      with open(self.file,'w') as file :
       file.write(self.data)
      return True 
     else :
      return self.saveAs()  
  """
  # Strictly speaking this should really lock and then open 
  # the main file and the temp file, compare their contents 
  # and open up a diff if there is a discrepency beyond the 
  # updates in the temp file, once resolved the both files 
  # ought to be resolved into the same file.
  temp = file = False
  try :
#    print(dir(self))
   print(self.temp)
#    with open(self.temp,'w') as file :
#     file.write(self.data)
#    temp = True
  except Exception as error:
   raise error # as IOError("The save function failed to store ones data")
  try :
#    with open(self.file,'w') as file :
#     file.write(self.data)
   file = True
  except Exception as error:
   raise error # as IOError("The save function failed to store ones data")
  return (temp, file)

#  @abstractmethod
 def saveAs(self, target):
  """
  This gets called when 
  """
  if isinstance(target, (Path, WindowsPath, PosixPath)): # Other classes to consider PurePosixPath, PureWindowsPath, PurePath (This seems to be the common decendant e.i. PuePath may be substituted for the list of classes)
   self.file = target
   return self.save()
  elif isinstance(source, str) and os.path.exists(source):
   self.file = Path(source).resolve()
   return self.save()
  else :
   raise ValueError("Expected TARGET to be a file name or path")
   # Originally I though the user might extract the data through this function but it is far easier for them to access self.data
#    self.data, self.file = source, None

# MainWidget = MainMDIWidget
