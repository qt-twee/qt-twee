class TextEditor(QTextEdit, MainWidget):

 formats = [Format(90, "Text", ["*.txt", "*.txt", "*.rtf"],0),
            Format(89, "JSON", ["*.json"],0),
            Format(89, "SQL",  ["*.sql"],0)]
 
 @property
 def data(self) :
  return self.text()
 
 @data.setter
 def data(self, data) :
  self.setText(data)


# Originally :
#
# class Text(MainWidget, QTextEdit) :
#  """
#  """
#  formats = [("Any File", ["*.*"])]
#  
#  @property
#  def html(self) :
#   return self.toHTML()
# 
#  @property
#  def data(self) :
#   # This should really respond to self.text() or the canonical inverse of self.setText()
#   try : 
#    return self.text()
#   except AttributeError as error:
# #    print(type(error), error)
# #    print([item for item in dir(self) if "to" in item.lower()]) 
#    return self.toPlainText()
#   # return self.text()
# 
#  @data.setter
#  def data(self, value) :
#   self.setText(value)
# 
#  def __init__(self, *args, **kvps) :
#   super().__init__(*args, **kvps)
# #   if file : 
# #    self.load(file)
# #   super(MainWidget, self).__init__(*args, file = file, **kvps)
# #   super(QTextEdit, self).__init__(*args, **kvps)
# 
#  def load(self, source):
#   load = lambda file : file.read()
#   if isinstance(source, (Path, WindowsPath)):
#    with source.open('r') as file :
#     self.data, self.file = load(file), source
#   elif isinstance(source, str) and os.path.exists(source):
#    with open(source,'r') as file :
#     self.data, self.file = load(file), Path(source)
#   elif isinstance(source, str) :
#    self.data, self.file = source, None
#   else : # At this point we have something other then text.
#    self.data, self.file = source, None
# 
#  def save(self):
#   if self.file : 
#    with open(self.file,'w') as file :
#     file.writelines(self.data)
#    return True 
#   else :
#    return self.saveAs()  
# 
#  def saveAs(self):
#   """
#   Select the file type to save the data in
#   """
#   if self.file is None:
#    file = QFileDialog.getSaveFileName(self, "Save File As", str(Path.cwd()), "Json File (*.json)")
#    if file[0]:
#     self.file = Path(file[0])
#   print(self.file)  
#   return True 
#   
# class Main(MainWindow) :
#  """
#  Example implementation of MainWindow
#  """
#  widgets = [Text]
# 
#  def __init__(self, *args, **kvps):
#   super().__init__(*args, **kvps)
# 
#  def new(self, *args, **kvps):
#   super().open(*args, **kvps)
# 
#  def open(self, *args, **kvps):
#   super().open(*args, **kvps)
# 
#  def save(self, *args, **kvps):
#   super().save(*args, **kvps)
# 
#  def saveAs(self, *args, **kvps):
#   super().__init__(*args, **kvps)