import overlays
import os
from pathlib import Path

class MainWidget() :

 def __init__(self, file = None):
  if file : self.file = file
  
 @property 
 def temp() : 
  if not hasattr(self, "_temp_") :
   if self.file :
    self._temp_ = self.file.temp() # (self.file.parent, self.file.stem, self.file.extn)
   else :
    self._temp_ = Path.tmppath
  return self._file_ 

 @property 
 def file(self) : 
  # Alternative implementation
#   if not hasattr(self, "_file_") :
#    self._file_ = "File"
#   return self._file_ 
  try :
   return self._file_
  except AttributeError :
   self._file_ = next(tempfiles())
  finally : 
   return self._file_

 @file.setter
 def file(self, path) : 
  # Not sure if it's prudent to both create and open the file 
  # or if it's sufficient to simply open it ?
  assert isinstance(path, Path)
  self._file_ = path
  _temp_, temp = path.temp(), self_temp_
  if self._temp_ : # Move any existing temp file to the new location
   try :
    os.rename(temp, _temp_)
    self._temp = _temp_
   except OSError as error: 
    # Either delete the file or create a copy.
    logging.error(error.message)  
  else : # 
   self._temp_ = _temp_
   

if __name__ :
 print(MainWidget().file)
 
#    if self.path :
# #    self._file_, self._path_ = mkstemp() 
#     self._file_ = self.path
#    else : 
#     self._file_ = tempfile.NamedTemporaryFile()
#    try : 
#     return self._temp_ 
#    except AttributeError :
#     self._temp_ = next(tempfile._get_candidate_names())
#     
#    finally :
#     return self._temp_

#  @file.setter
#  def file(self, file):
#   assert isinstance(file, (Path, PosixPath, WindowsPath)) # Ensure file name, note that it might not exist yet.
#   assert not file.is_dir()
#   self._file_ = file

# if __name__ == "__main__" :
#  print(PathBasedWidget().file)
 