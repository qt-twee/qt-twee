"""
Qt Widgets
==========

This is a simple example of a main application.
"""
# from twee.overlay import docopt, normalize, split, retag, clean, funparts, Path, WindowsPath
import sys, os
from pathlib         import Path
from pkgutil         import get_data
from PyQt5.QtWidgets import QApplication, QTextEdit
from PyQt5.QtGui     import QIcon, QPixmap
# from .abc import MainMDIWindow as MainWindow, MainMDIWidget as MainWidget, Format
from .abc        import MainWidget
from .cmd        import parser
# from .containers import Format
from .widgets    import MainMDIWindow as MainWindow
from .utilities  import extentions
from .           import __meta__
# from paste import reloader
# reloader.install()

# Application
app = QApplication(sys.argv)
# Branding
pix = QPixmap()
if pix.loadFromData(get_data(__package__,"favicon.ico"),'ico') :
 app.setWindowIcon(QIcon(pix))
app.setOrganizationDomain(__meta__.__website__)
app.setOrganizationName(__meta__.__company__)
app.setApplicationName(__meta__.__project__)
app.setApplicationVersion(".".join(str(i) for i in __meta__.__version__))
# Command line arguments
# - Docopt
# ops = docopt(cmd, app.arguments()[1:], help=False, options_first=True) # Need to disbale help and version, perhaps enable arguments first ?
# arg, kvp, rem = split(retag(ops, clean(ops)), *funparts(MainWindow))   # Note : docopt.normalize fails as Mainwindow is not a function but rather a class, so `self` appears in args but is not present in ops.
# - ArgParse
# opts = parser.parse_args(app.arguments()[1:])
# args, kvps = opts._get_args(), dict(opts._get_kwargs())
# print(parser.parse_known_args(sys.argv),parser.parse_args(sys.argv))
# - Qt CL Parser
parser.process(app)
# Entry Points
ext = extentions(__package__)
inc = parser.values("include") or ext.keys()
exc = parser.values("exclude")
ext = {k:v for k,v in ext.items() if k in inc and k not in exc}
if parser.isSet("formats") : 
 for key,val in ext.items() :
  print(key, ":\n ", "\n ".join(fmt.description() + " (" + ",".join(fmt.extentions) + ")" for fmt in val.formats))
else :
 # Main Window
 win = MainWindow(paths = [path.resolve() for path in map(Path,parser.positionalArguments()) if path.exists()], formats = [item for list in ext.values() for item in list])
 win.show()
 # Event Loop
 sys.exit(app.exec_())
