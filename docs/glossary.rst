--------
Glossary
--------

.. glossary ::

   :abbr:`API (Advanced Program Interface)` : API
     The interface used by programmers to utilize ones code base.

   module
     The atomic unit for Python source code e.g. :code:`a` or :code:`c` in :code:`b.c`.
     Traditionally this refers to a python file.
     Within the context of this document it largely excludes :file:`__init__.py` files.

   package
     A collection of one or more modules.
     Traditionally speaking, packages would map to the folders upon ones system; hence any folder containing one or more Python module(s) is a package.
     The package is identified as the first part of a modules' name e.g. :code:`a` and :code:`a.b` in :code:`a.b.c`.

