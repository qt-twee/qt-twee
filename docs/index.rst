.. Qt-Twee documentation master file, created by
   sphinx-quickstart on Tue Jul 10 09:29:44 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=====================
Application Framework
=====================

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Contents:
   
   usage
   vendors
   design
   twee
   packages
   glossary
   faq

|Project| provides a thin framework for Qt applications.
It provides a minimal application shell; performing the tasks necessary of a typical application.
Functionality is provided by third party packages which register a widget against specific file/mime type(s) under the |entrypoint| entry point.
Users may then manipulate files of the respective type within twee via the respective subclass.

Users should review the :ref:`Usage` and :ref:`Packages` sections and third parties the :ref:`Vendors` section.
:ref:`Design` details the implementation and :ref:`Twee` the |API|.

------------------
Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
