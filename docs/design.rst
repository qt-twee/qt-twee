------
Design
------

|Project| effectively provides two classes separately handling the aspects of a typical application :

 **MainWindow**
  The window class assosciating supported file types with their respective widgets.
  A thin wrapper around the Qt MainWindow class to provide file handling for ones MainWidget(s).
 **MainWidget**
  The widget class implementing the functionality for the target mime/file type.
  These are contributed entirely by third party packages. 
  Provides the core functionality of |Project| performing data manipulation for the file types they are registered against.

Internally these classes interact over a common protocol.
  
Main Window
===========

:class:`twee.MainWindow`, a thin subclass of :class:`QtWidgets.QMainWindow`, implements the minimal file handling functionality (New/Open/Save/Close) commonly required for an application.
Internally a :class:`QtWidgets.QtMdiArea` is setup as the central widget which manages :class:`QMainMDIWidget` which containt the :class:`twee.MainMDIWidget` subclasses.

.. inheritance-diagram :: twee.abc.MainWindow
  :caption: class inheritance diagram for :class:`MainWidget`

Main Widget
===========

.. inheritance-diagram :: twee.abc.MainWidget
  :caption: class inheritance diagram for :class:`MainWidget`

Vendors subclass :class:`twee.MainWidget`, itself a thin subclass of :class:`QWidget` that interfaces with :class:`twee.MainWindow`, within their package(s) and implement the logic necessary for their supported formats.
This subclass must then be registered under the |entrypoint| entrypoint making the users application available both as a standalone program invoked via their project or as a third party addon to |Project|.

Improvements
============

Some tasks still need to be carried uout uopn the project.

 * Consider the optional usage of the `Re-entry <https://pypi.org/project/reentry/>`_ package
