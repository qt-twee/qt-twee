----------------------
Command line interface
----------------------

.. automodule :: twee.cmd
  :members:         
  :undoc-members:   
  :show-inheritance:

.. note ::

   `SO <https://stackoverflow.com/a/21166631/958580>`_ discusses the use of argparse with Qt.
   There appears to be two means of documenting :class:`ArgumentParser` instances :mod:`sphinxcontrib.autoprogram` which seems to superceed :mod:`sphinxarg.ext`.

.. note ::

   Presently `:class:`QCommandLineParser` <http://doc.qt.io/qt-5/qcommandlineparser.html>`_ handles command line parsing for |Project|

.. note ::

   There is also the :mod:`sphinxcontrib.programoutput`