-------
Widgets
-------

.. contents :: twee.widgets
  :depth: 2

.. automodule :: twee.widgets
  :members:         
  :undoc-members:   
  :show-inheritance:
  :exclude-members: MainMDIWindow


Main Widget
===========

Main Window
===========

.. autoclass :: twee.widgets.MainMDIWindow
  :members:         
  :undoc-members:   



