--------
Packages
--------

|Project| relies upon third party packages for it's functionality.
This page provides as comprehensive a listing of such packages as possible.
If you are the author or a user of such a package and find it is not listed please log an issue at `Gitlab <www.gitlab.com>`_ and it will be included in due course.

.. note ::

   Qt-Icons provides a view script that seems to load SVG files.
