--------------------------
Frequently Asked Questions
--------------------------

Entry Point Errors
==================

I have encountered the following errors while developing third party packages for |Project|.
Typically the packages would be installed using the `pip install -e .` command and Python invoked from the projects' root folder.
::

  ModuleNotFoundError: No module named 'PACKAGE.MODULE.MainWidget'; 'PACKAGE.MODULE' is not a package
  
::

  ImportError: module 'PACKAGE.MODULE' has no attribute 'MainWidget'

These usually result after one has restructured/renamed either the package, module or class itself or altered the entrypoint registered within :file:`setup.py`.

One should clear out any remnant :file:`*.egg-info` files.
Python seems to inspect the dated versions of these files especially when invoked from the project root.