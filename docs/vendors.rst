-------
Vendors
-------

This section covers the extention of Twee by third party vendors.
It mostly provides a set of guidelines on the recommended practice.

Thir party packages must provide a :ref:`Main Widget` and register it within their :ref:`setup script`.

Naming
======

Projects that expressly target |Project| are best named ``twee-PROJECT`` to indicate this.
Larger projects, that merely utilize |Project|, would be named otherwise and should specify the appropriate :ref:`Classifiers`.

Main Widget
===========

Packages depending upon |Project| should subclass :class:`twee.abc.MainWidget`, itself a subclass of :class:`QtWidgets.QWidget`.
::

  from twee import abc, Format
  
  class MainWidget(abc.MainWidget):

   formats = [Format(100, "FILE FORMAT", ["*.EXT"], 0)]
   
While it is not *strictly* necessary to subclass :class:`twee.abc.MainWidget`. 
Ones :class:`MainWidget` must provide a :attr:`formats` attribute listing the :class:`Format`\ s that it supports. 
Ideally it should also implement :meth:`load`, :meth:`save` and :meth:`saveAs` methods.

.. graphviz ::
   :align:center
   
   graph mainwidget {
    a [label=":file:`file`"];
    b [label=":file:`data`"];
    a -> b;
   };
  

Setup Script
============

|Project| scans the entry points under the |entrypoint| entrypoint importing and registering the classes it finds as it does so.
This is the underlying mechanism that makes ones main widgets' available when executing `python -m twee`.
::

  setup( ...
    install_requires = ['Qt-Twee',],
    entry_points = {"twee":["PROJECT = PACKAGE.MODULE:MAINWIDGET"]}, 
    classifiers  = [...
       #"Framework :: Twee", # See the Vendors:Setup Script:Classifiers section under the Qt-Twee documentation before uncommenting
       ...],
    ...)
    
Where `PROJECT` is the name of ones project and :mod:`PACKAGE.MODULE` the import path for the :class:`MainWidget` sub/class it provides.

Classifiers
-----------

Should |Project| beccome big enough to earn it's own classifier then package authors should include the following classifier :: 

  Framework :: Twee

within their package metadata.

Registration
------------

Optionally, package writers may register their viewer with the |Project| entrypoint, :attr:`twee.views`.
One simply points the entrypoint to the :class:`twee.MainWidget`subclass within their projects :file:`setup.py` file.
::
 
  setup(...
   entrypoints = {
    "twee.views"}
   ...)

   Main function
=============

Often one sees the code necessary for initiating a Qt event loop wrapped into a :meth:`main` function.
|Project| does not supply such a :meth:`main` function since it is quite trivial to implement.
This is shown both for development in standalone modules and runnable packages.

When |Project| starts up it identifies the third party vendor :ref:`Packages` available upon ones system.

Modules
-------

Within modules it is necessary to guard against a standaed import or execution as a module.
::
  
  from twee import 

Packages
--------

When packages are executed directly Python will invoke the :file:`__main__.py` file.
Within this file :attr:`__name__` will always be set as `__main__` negating the need utility of the guard used previously.
::

  from Qt import QApplication
  from twee import MainWindow, parser
  from .MODULE import MAINWIDGET
  
  # Qt Eventloop
  app = QApplication(sys.argv)
  # Command line
  opts = parser.parse_args()
  # print(dir(opts))
  # # print(opts._get_args())
  # # print(opts._get_kwargs())
  # def check(*args, **kvps) :
  #  print(args)
  #  print(kvps)
  # check
  # Main Window
  win = MainWindow(*opts._get_args(),**dict(opts._get_kwargs()))
  win.show()
  # Run Application
  sys.exit(app.exec_())

