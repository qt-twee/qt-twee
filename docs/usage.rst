-----
Usage
-----

|Project| tries to make it as simple as possible for users to get started.

Installation
============

|Project| is most easily installed via pip as follows 
::
  
  pip install Qt-Twee

and subsequently removed with
::
  
  pip uninstall twee

Invocation
==========

.. highlight:: html

|Project| may be invoked directly from the command line as follows ::

  twee [file ...]

or indirectly, as a module, through the Python interpreter ::

  python -m twee [file ...]

The :ref:`Command line interface` section covers other possible invocations.
To use |Project| within ones own project see the :ref:`Main function` section.

Features
========

|Project| is dependant upon third party packages for it's functionality and one should review the available :ref:`Packages` accordingly.
The main window merely supports the following features:

 - Creating/Opening a file
 - Closing/Saving a file
 - File menu which one may extend
 - Menu bar which one may extend
